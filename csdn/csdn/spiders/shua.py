# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class ShuaSpider(CrawlSpider):
    name = 'shua'
    allowed_domains = ['csdn.net']
    start_urls = ['https://blog.csdn.net/qq_42259469']

    rules = (
        Rule(LinkExtractor(allow=r'https://blog.csdn.net/qq_42259469/article/details/84971995'), follow=False),
        Rule(LinkExtractor(allow=r'https://blog.csdn.net/qq_42259469/article/details/84892599'), follow=False),
        Rule(LinkExtractor(allow=r'https://blog.csdn.net/qq_42259469/article/details/84861293'), follow=False),
        Rule(LinkExtractor(allow=r'https://blog.csdn.net/qq_42259469/article/details/84784461'), follow=False),
        Rule(LinkExtractor(allow=r'https://blog.csdn.net/qq_42259469/article/details/84767207'), follow=False),
        Rule(LinkExtractor(allow=r'https://blog.csdn.net/qq_42259469/article/details/84646570'), follow=False),
    )

